#!/usr/bin/env sh
#2019/01/15
#Elia Fry
#ds4Setup.sh
#https://boilingsteam.com/dual-shock-4-a-good-choice-on-linux/

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

yay -S ds4drv

cd /etc/udev/rules.d/
wget http://boilingsteam.com/download/50-ds4drv.rules

udevadm control --reload-rules
udevadm trigger

cd ~/.config
wget http://boilingsteam.com/download/ds4drv.conf

echo "Done! Run with \"ds4drv\""
