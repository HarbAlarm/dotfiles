#!/usr/bin/env sh

sed -i 's/shadow = true/shadow = false/g' ~/.config/compton.conf
sed -i 's/fading = false/fading = true/g' ~/.config/compton.conf
