#!/usr/bin/env sh

count=50

oneko -name oneko0 -time 100000 &

for ((i = 0; i < ${count}; ++i ));
do
    toCat=${i}+1
    oneko -name oneko$((i+1)) -time 100000 -toname oneko${i} &
    sleep .2
done
