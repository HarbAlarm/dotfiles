**kvm passthrough:**
https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF#Prerequisites

In bios:
Enable vt-x & vt-d
Choose integrated GPU for host (iGPU)


/etc/default/grub
```bash
GRUB_CMDLINE_LINUX_DEFAULT="intel_iommu=on iommu=pt"
```

grub-mkconfig -o /boot/grub/grub.cfg

reboot

Check configuration:
sudo dmesg | grep -i -e DMAR -e IOMMU


Check/List groups:
```bash
#!/bin/bash
shopt -s nullglob
for g in /sys/kernel/iommu_groups/*; do
    echo "IOMMU Group ${g##*/}:"
    for d in $g/devices/*; do
        echo -e "\t$(lspci -nns ${d##*/})"
    done;
done;
```
If target GPU is in a group with a "PCI bridge", reseat your GPU into another PCI slot or read the wiki


Example ids: 10de:13c2,10de:0fbb

/etc/modprobe.d/vfio.conf
```bash
options vfio-pci ids=10de:13c2,10de:0fbb
```

Add to the BEGINNING of "MODULES"
Order matters!
/etc/mkinitcpio.conf
```bash
MODULES=(vfio_pci vfio vfio_iommu_type1 vfio_virqfd ...)
```

Make sure modconf exists in vfio.conf
HOOKS=(... modconf ...)

mkinitcpio -p [kernel name]
or
mkinitcpio -P

reboot

Check all for vfio:
dmesg | grep -i vfio

Check a driver for vfio:
lspci -nnk -d 10de:13c2


yay -S / sudo pacman -S
qemu libvirt ovmf virt-manager ebtables
# not mention in wiki, ebtables (firewall) for networking

/etc/libvirt/qemu.conf
```bash
nvram = [
"/usr/share/ovmf/x64/OVMF_CODE.fd:/usr/share/ovmf/x64/OVMF_VARS.fd"
]
```

sudo systemctl enable libvirtd virtlogd
sudo systemctl start libvirtd virtlogd


sudo usermod -a -G libvirt [user]

Debugging libvirtd:
journalctl -xe SYSTEMD_UNIT=libvirtd.service


# start virt-manager

# create vm
# choose "customise configuration before install"
# Change Overview>Firmware to UEFI.......
# Uncheck "CPUs>Copy host CPU configuration" and type "host-passthrough" into Model

# optional: add hardware drive to reduce IO overhead