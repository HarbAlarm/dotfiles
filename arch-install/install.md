**arch-install:**
```bash
# this cheatsheet assumes you have read the official guide: https://wiki.archlinux.org/index.php/Installation_guide
# it also assumes you are using a uefi to boot and uses /dev/vda as an example device

loadkeys de_CH-latin1

ls /sys/firmware/efi/efivars

ping archlinux.org

# unnecessary, might remove
timedatectl set-ntp true
timedatectl set-timezone Europe/Zurich

fdisk -l
or
lsblk

# for uefi boot, /dev/vda as example device
parted /dev/vda
mklabel gpt
mkpart efi fat32 1MiB 261MiB
set 1 esp on
mkpart primary ext4 261MiB [PARTITION-SIZE]
mkpart swap linux-swap [PARTITION-SIZE+261] 100%

mkfs.fat -F 32 /dev/vda1
mkfs.ext4 /dev/vda2
mkswap /dev/vda3
swapon /dev/vda3

mount /dev/vda2 /mnt
mkdir /mnt/efi
mount /dev/vda1 /mnt/efi


# If necessary edit /etc/pacman.d/mirrorlist

pacstrap /mnt base
# or include more packages
pacstrap /mnt base base-devel awesome i3-gaps gparted falkon


genfstab -U /mnt >> /mnt/etc/fstab
# check resulting file for errors

arch-chroot /mnt

ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

hwclock --systohc

# uncomment desired locales in /etc/locale.gen

locale-gen

# set main language
echo "LANG=en_GB.UTF-8" > /etc/locale.conf

echo "KEYMAP=de_CH-latin1" > /etc/vconsole.conf

echo "arch" > /etc/hostname

# add localhost domain to hosts
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1 localhost" >> /etc/hosts


# if using lvm, encryption or raid
mkinitcpio -p linux


passwd


# install a bootloader

# grub bios
pacman -S grub
grub-install --target=x86_64-efi --efi-directory=esp --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# grub uefi
pacman -S grub
grub-install --target=i386-pc /dev/sdX
grub-mkconfig -o /boot/grub/grub.cfg

# or refind (uefi only)
pacman -S refind-efi
refind-install
# more info required


# after installation and reboot:

useradd -m -g users <username>
passwd <username>

sudo visudo

# Find the following line
root ALL=(ALL) ALL
# Add the following underneath
<username> ALL=(ALL) ALL

# login as created user

sudo systemctl start dhcpcd
sudo systemctl enable dhcpcd

sudo pacman -S lightdm awesome xorg-server


# autologin:

sudo vim /etc/lightdm/lightdm.conf

# Find
# autologin-user=
# autologin-session=

# Replace
autologin-user=<username>
autologin-session=awesome

sudo groupadd -r autologin
sudo gpasswd -a <username> autologin

# NOT as root
systemctl enable lightdm

localectl set-x11-keymap ch

```