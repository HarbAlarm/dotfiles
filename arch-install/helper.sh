#!/usr/bin/env sh
#2019/01/19
#Elia Fry


# set default values

# argument variables
step=0
hostname=""
efi=false

sourceDir=$(dirname "$0")
cd $sourceDir

# function to report error messages
errMsg()
{
	echo ""; echo $1; echo ""; echo usage1; exit 1
}

usage()
{
    echo "Usage: helper [OPTION...]

-h, --help                  Print this help
-s, --step <STEP>           Run install code for specified step as a number
    1
	
	yay						install the yay package manager"
}


# test for correct number of arguments and get values
if [ $# -eq 0 ]
	then
	# help information
	usage
	exit 0
elif [ $# -gt 16 ]
	then
	errMsg "--- TOO MANY ARGUMENTS WERE PROVIDED ---"
else
	while [ $# -gt 0 ]
		do
			# get parameter values
			case "$1" in
		        -h|--help)    # help information
					   usage
					   exit 0
					   ;;
				-s|--step)		# get hostname
						shift  # to get the next parameter
						errorMsg="--- INVALID HOSTNAME SPECIFICATION ---"
						step=`echo "$1" | tr "[:upper:]" "[:lower:]"`
				;;
				-n|--hostname)
                        shift
						hostname=`echo "$1" | tr "[:upper:]" "[:lower:]"`
				;;
				 -)    # STDIN and end of arguments
					   break
				;;
				 -*)    # any other - argument
					   errMsg "--- UNKNOWN OPTION ---"
				;;
		     	 *)    # end of arguments
					   break
				;;
			esac
			shift   # next option
	done
fi

if [ -d /sys/firmware/efi/efivars ]; then
	efi=true
fi

case "$step" in
    0) echo "A true first step, get a cup of tea." ;;
    1)
		loadkeys de_CH-latin1
		timedatectl set-ntp true
		timedatectl set-timezone Europe/Zurich

		if $efi; then 
			echo "heads up, booted through UEFI"
			echo "
#now set up your partitions using parted or fdisk
		
#Example using parted (based on [UEFI]/BIOS):
lsblk
parted /dev/sdX
mklabel gpt
mkpart efi fat32 2MiB 261MiB
set 1 esp on
mkpart primary ext4 261MiB [PARTITION-SIZE]
#PARTITION-SIZE should leave about 8GB to account for swap space
mkpart swap linux-swap [PARTITION-SIZE+261] 100%
quit

mkfs.fat -F 32 /dev/vdX1
mkfs.ext4 /dev/vdX2
mkswap /dev/vdX3
swapon /dev/vdX3

mount /dev/vdX2 /mnt
mkdir /mnt/efi
mount /dev/vdX1 /mnt/efi
"
		else echo "heads up, booted through BIOS"
echo "
#now set up your partitions using parted or fdisk
		
#Example using parted (based on UEFI[BIOS]):
lsblk
parted /dev/sdX
mklabel msdos
#PARTITION-SIZE should leave about 8GB to account for swap space
mkpart primary ext4 2MiB [PARTITION-SIZE]
mkpart swap linux-swap [PARTITION-SIZE+261] 100%
quit

mkfs.ext4 /dev/vdX1
mkswap /dev/vdX2
swapon /dev/vdX2

mount /dev/vdX1 /mnt
mkdir /mnt/efi
"
		fi
	;;
	2)
	echo "
You may want to edit /etc/pacman.d/mirrorlist

if not already mounted, mount your primary partition at /mnt and execute:
pacstrap /mnt base base-devel xorg-server lightdm lightdm-webkit2-greeter awesome alsa grub os-prober vim
genfstab -U /mnt >> /mnt/etc/fstab
'helper -x 2y' to execute above 2 commands
you may add/remove packages as you wish"
	;;
	2y)
		pacstrap /mnt base base-devel xorg-server lightdm lightdm-webkit2-greeter awesome alsa grub os-prober vim
		genfstab -U /mnt >> /mnt/etc/fstab
	;;
	3)
		arch-chroot /mnt

		ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

		hwclock --systohc


		echo "uncomment desired locales in /etc/locale.gen"
	;;
	4)
		locale-gen

		echo "LANG=en_GB.UTF-8" > /etc/locale.conf

		echo "KEYMAP=de_CH-latin1" > /etc/vconsole.conf

		echo "arch" > /etc/hostname

		echo "127.0.0.1 localhost" >> /etc/hosts
		echo "::1 localhost" >> /etc/hosts

		passwd
	;;
	5)
		mkinitcpio -p linux
		echo "now the bootloader"
	;;
	6)
		echo "
these are probably switched..:

# grub bios
grub-install --target=x86_64-efi --efi-directory=esp --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# grub uefi
grub-install --target=i386-pc /dev/sdX
grub-mkconfig -o /boot/grub/grub.cfg"
	;;
	placeholder)
		if [ -d /sys/firmware/efi/efivars ]; then 
			echo "efi"
		else echo "no efi"
		fi
	;;
	yay)
	git clone https://aur.archlinux.org/yay.git /tmp/yay
	cd /tmp/yay
	makepkg -si
	cd $sourceDir
    ;;
    *) usage ;;
esac



exit 0