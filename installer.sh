#!/usr/bin/env sh

#   Harb's installer
#   /   /   /   /   /
#    /   /   /   /

# This file contains install instructions for various tools that I like to use on arch linux

# echo "did you un/comment what you (don't) need?"
# exit 0

# Comment out the following lines as needed

# Basic tools
# sudo pacman -S --needed --noconfirm git vim fish sxhkd rofi screen usbutils cpulimit

# Install AUR helper
# TODO: if not installed
# git clone https://aur.archlinux.org/yay.git /tmp/yay-install && pushd /tmp/yay-install && makepkg -si && popd
# yay -S paru

# install nix if missing
if ! command -v nix &> /dev/null; then
sh <(curl -L https://nixos.org/nix/install) --daemon
fi

# or arch nix install
# yay -S nix
# sudo usermod -aG nix-users harb
# sudo usermod -aG seat harb
# sudo systemctl enable nix-daemon

# nix shell --extra-experimental-features "nix-command flakes" gitlab:HarbAlarm/dotfiles#homeConfigurations.harb.activationPackage
nix-shell -p home-manager --run "home-manager --extra-experimental-features \"nix-command flakes\" switch" & \

# missing from nixpkgs
# yay -S --needed --noconfirm

# don't work through nix
yay -S --needed --noconfirm hyprland-git hy3-git swaylock-effects; \
yay -S --needed --noconfirm kitty



# Audio
# yay -S --needed --noconfirm pipewire pipewire-pulse pipewire-jack pavucontrol
# yay -S --needed --noconfirm pulseaudio pavucontrol

# Bluetooth
# yay -S --needed --noconfirm bluez bluez-utils blueman

# WMs
# yay -S hyprland-git hy3-git
# yay -S --needed --noconfirm i3-gaps # awesome herbstluftwm

# Wayland tools
# TODO

# Xorg tools
#  yay -S --needed --noconfirm xdotool arandr xorg-xinput xorg-xkill xorg-xev scrot

# Terminal Emulator/s
# yay -S --needed --noconfirm kitty #sakura alacritty

# WM tools
# yay -S --needed --noconfirm picom-tryone-git deadd-notification-center-bin polybar i3lock-color perl-anyevent-i3 powerline powerline-fonts nitrogen wmctrl clipmenu

# Themes
# yay -S --needed --noconfirm breeze-gtk arc-gtk-theme

# Icons
# yay -S --needed --noconfirm breeze-icons

# Cursors
# yay -S --needed --noconfirm breezex-cursor-theme oxy-neon hacked-aio-righty

# Terminal Tools
# yay -S --needed --noconfirm ranger dnsutils ddgr rclone tmsu tldr mpd ncmpcpp lynx lsd broot imagemagick unzip shellcheck pfetch pv progress

# Browsers
# yay -S --needed --noconfirm firefox-esr firefox chromium # vivaldi falkon

# VPN
# yay -S --needed --noconfirm windscribe-cli 

# Password Manager
# bitwarden bitwarden-cli-bin bitwarden-rofi

# UI Tools
# yay -S --needed --noconfirm docker gimp inkscape gparted libreoffice mpc mpv celluloid etcher seahorse sonata diodon xarchiver

# Other
# yay -S --needed --noconfirm refind-efi-bin heimdall-git isousb screenkey zotero drawio-desktop anydesk-bin

# Messaging
# yay -S --needed --noconfirm telegram-desktop discord

### Coding
# yay -S --needed --noconfirm vscodium ruby-sass # atom geany mousepad 

### Fun Stuff
# yay -S --needed --noconfirm neofetch ufetch cava sonic-visualiser sl lolcat figlet oneko pipes.sh gource cool-retro-term

### Additional
# yay -S --needed --noconfirm streamlink youtube-dl

# Fonts / Error Correction
# yay -S --needed --noconfirm nerd-fonts-complete python-fontawesome hunspell-en_GB

# Use doas instead of sudo
# WARNING: this should work, but please verify /etc/doas.conf before installing doas (thereby removing sudo), ask me how I know
# echo "permit nopass $USER as root" | sudo tee -a /etc/doas.conf && sudo chmod 600 /etc/test.conf && yay -S --needed --noconfirm opendoas opendoas-sudo

# Gaming
#yay -S --needed --noconfirm steam-native gamehub-git factorio
