import json


def readfile(path):
    with open(path, 'r') as file:
        data = file.read()
    return json.loads(data)


def writefile(path, data):
    with open(path, 'w') as outfile:
        json.dump(data, outfile)


def set_setting(json_in, s, value):
    # print(s)
    if 'left' not in s:
        if len(s) > 0:
            return set_setting(json_in, {'left': s, 'done': [], 'prefScoped': json_in}, value)
    else:
        if len(s['done']) == 0:
            searchArea = json_in
        else:
            searchArea = s['prefScoped']

        searchTerm = s['left'][0]
        # print("searchTerm: ", searchTerm)
        # print("searchArea: ", searchArea)

        if searchTerm in searchArea:
            s['prefScoped'] = searchArea[searchTerm]

        s['done'].append(s['left'][0])
        s['left'].pop(0)

        # if len(s) > 0 and s['left'][0] in prefs[s['done'][0]]:
        if len(s['left']) > 0 and searchTerm in json_in:
            return set_setting(json_in[searchTerm], s, value)
        elif searchTerm in searchArea:
            json_in[searchTerm] = value
            # return s['prefScoped']
            return True
        else:
            return False


def msg_error(key):
    key = '"' + key + '"'
    print(key, " missing from preferences?")


def setp(json_in, s, value):
    if not set_setting(json_in, s.copy(), value):
        objs = ""
        for obj in s:
            if objs == "":
                objs = obj
            else:
                objs += " -> " + obj
        msg_error(objs)
