#!/usr/bin/env sh
# 2024-08-03
# HarbAlarm


if [ $# -eq 0 ]
         then
         # help information
    echo "run harbMount.sh with a dir to be added to fstab"
    echo "example:"
    echo "harbMount.sh /run/media/harb/mynewdevice"
    exit 0
fi

printf '%s%s 0 0\n' '/dev/disk/by-uuid/' \
    "$(findmnt -n -o UUID,TARGET,FSTYPE,OPTIONS $1)" | sudo tee -a /etc/fstab


