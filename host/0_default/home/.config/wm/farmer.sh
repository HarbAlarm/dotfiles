#!/usr/bin/env sh

do6b4=false
do98click=false

# test for correct number of arguments and get values
if [ $# -eq 0 ]
	then
	# help information
   echo ""
   echo usage2
   exit 0
elif [ $# -gt 16 ]
	then
	errMsg "--- TOO MANY ARGUMENTS WERE PROVIDED ---"
else
	while [ $# -gt 0 ]
		do
			# get parameter values
			case "$1" in
		        -h)    # help information
					   echo ""
					   echo usage2
					   exit 0
					   ;;
				-d)		# do it
						do6b4=true
				;;
				-c)		# do it
						do98click=true
				;;
				 -)    # STDIN and end of arguments
					   break
				;;
				 -*)    # any other - argument
					   errMsg "--- UNKNOWN OPTION ---"
				;;
		     	 *)    # end of arguments
					   break
				;;
			esac
			shift   # next option
	done
fi




click4() {
    click
    click
    click
    click
}
click2() {
    click
    click
}
click() {
    xdotool mousedown 1
    sleep .2
    xdotool mouseup 1
    sleep .1
}


do2b4() {
    xdotool mousemove_relative -- 170 80
    click4
    xdotool mousemove_relative -- 170 80
    click4
    xdotool mousemove_relative -- 170 80
    click4

    xdotool mousemove_relative -- -170 80
    click4

    xdotool mousemove_relative -- -170 -80
    click4
    xdotool mousemove_relative -- -170 -80
    click4
    xdotool mousemove_relative -- -170 -80
    click4

    xdotool mousemove_relative -- 170 -80
}

do6b4() {
    lt=6
    rt=2
    i=1

    while [ $i -le $rt ]; do
        
        j=0
        while [ $j -lt $lt ]; do
            if [ $i -gt 1 ] || [ $j -gt 0 ]; then
                xdotool mousemove_relative -- -170 88
                # sleep .1
                click4
                # echo a $i $j
            fi
            ((j++))
        done
        # printf "\n"
        

        if [ ! $i -eq $rt ]; then
            xdotool mousemove_relative -- 170 88
            j=0
            while [ $j -lt $lt ]; do
                xdotool mousemove_relative -- 170 -88
                # echo b $i $j
                ((j++))
            done
        fi
        # printf "\n"
        ((i++))
    done
}

do98click() {
    i=0
    while [ $i -lt 98 ]
    do
        xdotool mousedown 1
        sleep .2
        xdotool mouseup 1
        sleep .01
        ((i++))
    done
}






if ($do6b4); then
    do6b4
fi

if ($do98click); then
    do98click
fi
