#!/bin/sh

export PATH="$PATH:/home/$USER/.nix-profile/bin"
export XDG_DATA_DIRS="$HOME/.local/share/applications:$HOME/.nix-profile/share"

rofi=rofi
path=/home/harb/.nix-profile/bin/rofi

if ( pidof "$rofi" ); then
    kill $(pidof $rofi)
else
    LC_ALL=C $path -show drun
fi

