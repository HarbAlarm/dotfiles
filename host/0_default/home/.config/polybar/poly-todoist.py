import importlib, requests, json
from datetime import datetime

print('disabled')
exit()

token = open('token').read()

tasks = {}
chosenOne = ''

r = requests.get(
    "https://api.todoist.com/rest/v1/tasks",
    headers={
        "Authorization": "Bearer %s" % token
    }
).json()


for obj in r:
    chosenDate = ''
    objDate = ''

    if (obj['completed'] is False):
        if (chosenOne == '' and 'due' in obj):
            chosenOne = obj

        elif ('due' in obj and 'due' in chosenOne):
            if ('datetime' in chosenOne['due']):
                chosenDate = datetime.strptime(chosenOne['due']['datetime'], '%Y-%m-%dT%H:%M:%SZ')
            else:
                chosenDate = datetime.strptime(chosenOne['due']['date'], '%Y-%m-%d')

            if ('datetime' in obj['due']):
                objDate = datetime.strptime(obj['due']['datetime'], '%Y-%m-%dT%H:%M:%SZ')
            else:
                objDate = datetime.strptime(obj['due']['date'], '%Y-%m-%d')

            if (objDate < chosenDate):
                # if (objDate > datetime.now()):
                    chosenOne = obj

print('todo: ', chosenOne['content'])
