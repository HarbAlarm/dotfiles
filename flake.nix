{
  description = "Home Manager configuration of Harb";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    # nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    chaotic.url = "github:chaotic-cx/nyx/nyxpkgs-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # hyprland.url = "github:hyprwm/Hyprland";
    # hyprland.url = "github:hyprwm/Hyprland/b08b72358ad549fd066e5be0fc3aa4c9df367607";
    # hyprland.packages.${pkgs.system}.hyprland.override = {legacyRenderer = true;};

    hyprland.url = "github:hyprwm/Hyprland";

    # hyprcontrib = {
    #     url = "github:hyprwm/contrib";
    #     inputs.hyprland.follows = "hyprland";
    # };

    # hyprland-plugins = {
    #     url = "github:hyprwm/hyprland-plugins";
    #     inputs.hyprland.follows = "hyprland";
    # };

    # hy3 = {
    #   url = "github:outfoxxed/hy3";
    #   inputs.hyprland.follows = "hyprland";
    # };

    # nixpkgs-wayland.url = "github:nix-community/nixpkgs-wayland";
  };

  # outputs = { nixpkgs, chaotic, home-manager, hyprland, /* hy3, */ ... } @ inputs:
  #   let
  #     system = "x86_64-linux";
  #     # pkgs = nixpkgs.legacyPackages.${system};
  #     pkgs = import nixpkgs {
  #       system = "${system}";
  #       config.allowUnfree = true;
  #     };
  #   in {

  # outputs = { nixpkgs, chaotic, home-manager, hyprland, shared, ... }: {
    
  outputs = { nixpkgs, chaotic, home-manager, hyprland, ... } @ inputs:
    let
      shared = [
      ./hyprland.nix
      ./home.nix
      ];
    in {
    homeConfigurations."elia" = home-manager.lib.homeManagerConfiguration {
      # inherit pkgs;
      pkgs = nixpkgs.legacyPackages.x86_64-linux;


      # Specify your home configuration modules here, for example,
      # the path to your home.nix.
      modules = [
        # hyprland.packages.${pkgs.system}.hyprland.override = {legacyRenderer = true;};
        hyprland.homeManagerModules.default {
          wayland.windowManager.hyprland = {
            enable = true;
            xwayland.enable = true;
            # plugins = [ hy3.packages.x86_64-linux.hy3 ];
          };
        }

        ./hyprland.nix
        ./home.nix
        chaotic.homeManagerModules.default

        {
          # Home Manager needs a bit of information about you and the paths it should manage.
          home.username = "harb";
          home.homeDirectory = "/home/elia";
        }
      ];

      # Optionally use extraSpecialArgs to pass through arguments to home.nix
    };
    homeConfigurations."harb" = home-manager.lib.homeManagerConfiguration {
      # inherit pkgs;
      pkgs = nixpkgs.legacyPackages.x86_64-linux;


      # Specify your home configuration modules here, for example,
      # the path to your home.nix.
      modules = [
        # hyprland.packages.${pkgs.system}.hyprland.override = {legacyRenderer = true;};
        hyprland.homeManagerModules.default {
          wayland.windowManager.hyprland = {
            enable = true;
            xwayland.enable = true;
            # plugins = [ hy3.packages.x86_64-linux.hy3 ];
          };
        }

        ./hyprland.nix
        ./home.nix
        # chaotic.homeManagerModules.default
        # shared

        {
          # Home Manager needs a bit of information about you and the paths it should manage.
          home.username = "harb";
          home.homeDirectory = "/home/harb";
        }
      ];

      # Optionally use extraSpecialArgs to pass through arguments to home.nix
    };
  };
}
