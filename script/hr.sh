#!/usr/bin/env sh
# https://www.reddit.com/r/commandline/comments/sdy47y/comment/hugvcxv/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button

i=1; while [ $i -le $(tput cols) ] ; do i=$((i+1)); printf "%s" "${1:-=}"; done ;
