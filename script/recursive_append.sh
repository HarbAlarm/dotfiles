#!/usr/bin/env sh
#2024/08/15 04:20
#Elia Fry

source_dir=$1
target_dir=$2
root_script=${3-true}
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

mkdir -p "$target_dir"
[ $root_script = true ] && echo ""

for file in "$source_dir"/.??* "$source_dir"/*; do
    [[ -e "$file" ]] || continue # skip nonexistant/nonreadable files
    echo -ne "$source_dir -> $target_dir \r"

    basename=$(basename "$file")

    if [[ -L "$file" ]]; then
        ln -sf "$(readlink "$file")" "$target_dir/$basename"
    elif [[ -d "$file" ]]; then
        $script_dir/recursive_append.sh "$file" "$target_dir/$basename" false
    elif [[ -f "$file" ]]; then
        if [[ -f "$target_dir/$basename" ]]; then
            echo "" >> "$target_dir/$basename"
            cat "$file" >> "$target_dir/$basename"
        fi
    fi
done

# if [ $root_script = true ]; then
#     $script_dir/hr.sh
#     echo "recursive append complete!"
#     echo ""
# fi
